# Tracking PathOS

## Généralités

## CR réunions

### Schéma causalité et rendez-vous
- Intégrer les remarques de Tommaso sur le schéma miro https://miro.com
- Faire un rendez-vous avec Ioanna, Harris et Petros à partir du 26/02 pour expliquer le nouveau schéma causal et les limitations de l'approche contrefactuelle
- Passer les nouveaux devis à Paul
- Relancer la DPO (contrat de sous-traitance) et SPV :`(` 

### Organisation des indicateurs d'impact sociétal
Tommaso liste les indicateurs et les personnes assignées ici : https://docs.google.com/spreadsheets/d/1wH6ySvMM4-dTNkRFGWpE03mZDJK2IsZK2r85zii5Ums/edit#gid=0 
